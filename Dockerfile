FROM celedhrim/kodi-server:krypton
LABEL org.freenas.interactive="false" \
      org.freenas.version="17.1" \
      org.freenas.upgradeable="true" \
      org.freenas.expose-ports-at-host="true" \
      org.freenas.autostart="true" \
      org.freenas.web-ui-protocol="http" \
      org.freenas.web-ui-port="8080" \
      org.freenas.web-ui-path="web" \
      org.freenas.port-mappings="8080:8080/tcp" \
      org.freenas.volumes="[						\
          {								\
              \"name\": \"/opt/kodi-server/share/kodi/portable_data\",					\
              \"descr\": \"Data volume\"				\
          }								\
      ]"
ADD advancedsettings.xml /opt/kodi-server/share/kodi/portable_data/userdata/
